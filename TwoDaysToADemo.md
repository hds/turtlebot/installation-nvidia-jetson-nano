# Two Days to a Demo

NVIDIA propose un [tutoriel](https://developer.nvidia.com/embedded/twodaystoademo) pour la prise en main de ses Jetson.
Celui-ci est assez long à suivre, et se divise en plusieurs étapes.

Il propose non seulement une introduction à l'utilisation de jetson-inference, mais aussi, ce qui nous intéressera particulièrement, à l'utilisation avec ROS.

## Hello AI World

[Hello AI World](https://github.com/dusty-nv/jetson-inference#hello-ai-world) est une première étape pour l'utilisation de la Jetson Nano et de Jetson-inference.

Si la NVIDIA Jetson Nano a été flashée avec l'image fournie par NVIDIA comme conseillé dans le [README](./README.md), elle est prête à accueillir l'environnement de travail Jetson-inference.

### Installation

L'installation de Jetson-inference se fait directement depuis les sources, comme décrit sur [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/building-repo-2.md).

Il est conseillé d'installer les paquets pour le développement sous python3 : `python3-numpy` et `libpython3-dev`. Il est utile d'installer `PyTorch`, il nous servira plus tard pour réentraîner des réseaux de neurones.

Dans le cas de la Jetson Nano, les résultats du build seront situés dans le dossier `build/aarch64`.
Celui-ci contient les exécutables des différents programmes que l'on pourra lancer, ainsi que les données de test nécessaires.

### Classification avec ImageNet

Le réseau ImageNet est utilisé pour la classification d'images.
Il prend en entrée une image, et détermine avec une certaine probabilité la classe de l'objet présent sur l'image.
Une image de sortie est aussi générée.
Il s'agit de l'image d'origine avec la classe de l'image et la probabilité écrits dessus.

L'utilisation du réseau est documentée sur [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-console-2.md).
Le réseau peut être appelé soit en console, soit lancé en live sur la caméra (voir [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-camera-2.md)) branchée à la Jetson Nano.

Le tutoriel montre également des comment écrire soit même un programme de reconnaissance d'images, faisant appel à l'api de tensorflow.

### Détection avec DetectNet

Le réseau DetectNet est utilisé pour la détection d'objets sur une image.
Il va chercher une classe d'objet particulière, et en trouver toutes les occurrences sur une image, ainsi que leurs coordonnées.

Voir [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/detectnet-console-2.md) pour la détection en console sur des images et [celle-ci](https://github.com/dusty-nv/jetson-inference/blob/master/docs/detectnet-camera-2.md) pour la détection sur le live de la caméra.

### Réentraînement avec PyTorch

PyTorch est une bibliothèque python permettant de faire du transfert de connaissances.
Le but est de réentraîner un réseau de neurones sur une banque de données différentes, ce qui sera bien plus rapide que de reconstruire un réseau de neurones.

Cet entraînement est beaucoup moins coûteux en ressources que l'entraînement d'un nouveau réseau, et même s'il est plus rapide de le faire sur un PC plus performant ou même sur un serveur dédié, il est possible de faire des petits entraînements directement sur la Jetson Nano.
C'est ce que nous allons faire maintenant.

Commencer par suivre [ces instructions](https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-transfer-learning.md) pour installer ce qui est nécessaire sur la Jetson Nano. 

Ensuite, [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-cat-dog.md) propose un exemple : il s'agit de réentraîner le réseau `resnet18` sur une banque de données de chats et de chiens pour construire un modèle.
Cet entraînement se fait en 35 passes (par défaut) sur une banque de 5000 images d'entraînement et 1000 images de validation (plus des images de test pour l'utilisateur).
L'entraînement complet est très long et à faire tourner en temps masqué.

À noter que le résultat n'est précis qu'à 80% d'après la page de documentation.
Il est donc probable que le réseau confonde des chats et des chiens lors des tests.

Le tutoriel propose ensuite un réentraînement sur un dataset contenant plus de classes.
[La page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-plants.md) est intéressante à lire, mais ce n'est pas la peine de perdre du temps à lancer le programme d'entraînement, c'est exactement la même chose que pour les chats et les chiens.

Jusqu'ici, l'entraînement n'a été fait que sur des banques de données téléchargées.
[Cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-collect.md) propose des instructions et des conseils pour la création de sa propre banque de données.

## Deploying Deep Learning

Une fois que l'on a suivi le tutoriel « Hello AI World », on est prêt à entrer dans la cour des grands avec le tutoriel « Deploying Deep Learning »

### Mise en place de DIGITS

Comme on a pu le voir lors du réentraînement sur la base de chats et de chiens, le Jetson Nano est assez lent pour l'entraînement de réseau.
Par ailleurs, cela le fait beaucoup chauffer, et, dans le cas où il est embarqué sur un robot, cela nous paralyse.

On va donc utiliser le Workflow DIGITS (Deep Learning GPU Training System) pour entraîner des réseaux en dehors du Jetson, et récupérer les resultats sur celui-ci par la suite.
On va donc monter un serveur d'entraînement avec DIGITS, et laisser à Jetson-inference et à la Jetson Nano le soin d'utiliser les réseaux entraînés par la suite.
Voir [cette page](https://github.com/dusty-nv/jetson-inference/blob/master/docs/digits-workflow.md) pour des explications plus détaillées.

#### Installation de DIGITS

On va donc installer DIGITS sur notres station de calcul.
Celle-ci peut être un serveur cloud ou un PC avec un bon GPU.
Lors de la réalisation de ce tutoriel, le serveur a été installé sur un HP ZBook17 G5 équipé d'une carte NVIDIA Quadro P4200.

On va installer dessus Ubuntu 16.04, ainsi que NVIDIA GPU Cloud et DIGITS.
Les instructions pour l'installation sont disponibles [ici](https://github.com/dusty-nv/jetson-inference/blob/master/docs/digits-setup.md).

*Lors de l'installation d'Ubuntu, il peut être impossible de démarer l'ISO ou l'os une fois fini.
Dans ce cas, penser à démarer en `nomodeset`, l'erreur pouvant venir d'un problème de reconnaissance du GPU au démarrage.
Cela sera réglé par la suite lors de l'installation des drivers NVIDIA*.

La mise en place de DIGITS se fait via Docker.
Pour cette mise en place, on bind sur le conteneur deux dossiers, dont le dossier `data`, qui ira dans le `/data` du conteneur.
Pour le bon fonctionnement de ce que l'on fera par la suite, on ne va pas mettre se volume en lecture seule (option `ro`).
Le lancement du conteneur devient alors :

```console
$ nvidia-docker run --name digits -d -p 8888:5000 \
 -v /home/username/data:/data
 -v /home/username/digits-jobs:/workspace/jobs nvcr.io/nvidia/digits:18.05
```

#### Installation de la carte et utilisation des réseaux

Les pages suivantes du tutoriel expliquent comment installer ce qu'il faut sur la carte Jetson Nano et comment utiliser ImageNet.
Comme nous avons déjà fait cela, nous passerons directement à la suite

### Entraînement d'ImageNet

#### Création d'un modèle

Les modèles de reconnaissance AlexNet et GoogleNet téléchargés par défaut avec DIGITS sont entraînés sur la base ILSVRC12, contenant environ un million deux cent mille images.

Nous allons utiliser DIGITS pour réentraîner GoogleNet avec des groupes de classes.
Ainsi, nous allons utiliser 230 classes de ILSVRC12 pour générer 12 classes et réentraîner le réseau.

Pour cela, il faut commencer par télécharger la base ILSVRC12.
Les instructions pour le faire sont disponibles [ici](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-training.md#downloading-image-recognition-dataset).
Placer le dossier contenant ces téléchargements dans le dossier `data` (celui qui est monté dans le `/DATA` du conteneur sur l'ordinateur tournant DIGITS).

**Attention !** La base de données ILSVRC12 est un ensemble de liens sur lesquels récupérer les images.
Nous alons donc ici télécharger environ 100Go d'images depuis plus de 1 million de liens.
Cela va donc être très très long (compter 5 à 6 heures sur le réseau UTC).

Ensuite, on va personaliser les classes d'objets, en en regroupant certaines (les félins vont devenir des chats par exemple, on va regrouper les fruits...).
Ce regroupement se fait par l'utilisation de liens symboliques, ce qui peut poser problème lors de l'utilisation de Docker.
On va donc suivre [ces instructions](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-training.md#customizing-the-object-classes), mais à l'intérieur du conteneur.
Dans un shell sur la machine faisant tourner DIGITS :

```console
$ docker exec -it digits bash
$ cd /data
$ wget https://rawgit.com/dusty-nv/jetson-inference/master/tools/imagenet-subset.sh
$ chmod +x imagenet-subset.sh
$ mkdir 12_classes
$ ./imagenet-subset.sh /data/ilsvrc12 12_classes
```
Si le dataset est dans le dossier ilsvrc12.

Suivre la fin des instructions de la page pour instancier le dataset (20 min) et lancer l'entraînement du modèle (5h), en adaptant les chemins au besoin *(lors de la création du datasetl, utiliser les chemins du conteneur : `/data/12_classes`)*.

#### Utilisation du modèle

Maintenant que l'on a fait un modèle de classification d'image en utilisant GoogleNet et notre propre dataset, on va l'importer sur le Jetson Nano et l'utiliser.
Pour cela, on commence par télécharger le modèle dans `~/jetson-inference/data/networks` sur le Jetson Nano, en suivant [ces instructions](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-snapshot.md).

Ensuite, on peut utiliser ce modèle avec `imagenet-console` comme montré [ici](https://github.com/dusty-nv/jetson-inference/blob/master/docs/imagenet-custom.md).
