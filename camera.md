# Utilisation de la caméra

Après avoir installé le Jetson Nano, avoir utilisé ROS pour les actions de base du Turtlebot3 et s'être initiés à l'inférence avec Jetson-inference, on va essayer de rejoindre la reconnaissance d'image avec ROS, via l'utilisation de nœuds pour utiliser la caméra et faire de la reconnaissance d'image.

## Publication de la caméra

La caméra embarquée sur le robot est une caméra csi, et on ne peut pas utiliser `uvc_camera` pour la faire fonctionner.
On va donc se servir de nœuds utilisant le framework multimedia GStreamer pour publier ce que voit la caméra sur un topic ROS.

Pour cela, on utilisera [gscam](https://github.com/ros-drivers/gscam), et [jetson_csi_cam](https://github.com/peter-moran/jetson_csi_cam), qui simplifiera le lancement du premier.
Pour l'installation, suivre les instructions données pour le paquet [jetson_csi_cam](https://github.com/peter-moran/jetson_csi_cam).

Pour faire fonctionner la caméra, on va modifier un petit peu le pipeline GStreamer que `jetson_csi_cam` donne l'ordre à `gscam` d'utiliser.
Dans le fichier `jetson_csi_cam.launch`, on effectue donc la modification suivante : 

```diff
@@ -15,9 +15,10 @@
   <param name="$(arg cam_name)/target_fps" type="int" value="$(arg fps)" />
 
   <!-- Define the GSCAM pipeline -->
-  <env name="GSCAM_CONFIG" value="nvcamerasrc sensor-id=$(arg sensor_id) ! video/x-raw(memory:NVMM),
-    width=(int)$(arg width), height=(int)$(arg height), format=(string)I420, framerate=(fraction)$(arg fps)/1 ! 
-    nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR" />
+
+  <env name="GSCAM_CONFIG" value="nvarguscamerasrc ! video/x-raw(memory:NVMM),
+    width=(int)$(arg width), height=(int)$(arg height), format=(string)NV12, framerate=(fraction)$(arg fps)/1 ! 
+    nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format(string)BGRx" /> 
```

On peut ensuite lancer le nœud ROS sur la jetson nano :

```console
$ roslaunch jetson_csi_cam jetson_csi_cam.launch width:=640 height:=640 fps=:15
```

Puis sur le PC, on lance `rqt_image_view`.
La jetson nano publie ses images sur les topics `/csi_cam_0/image_raw/`, `/csi_cam_0/image_raw/compressed/`

## Reconnaissance d'images

Pour la reconnaissance d'images avec ROS, on utilisera le nœud [ros_deep_learning](https://github.com/dusty-nv/ros_deep_learning), que l'on installera sur la jetson nano.

À l'heure où ce tutoriel est écrit, ce nœud n'est pas compatible avec la dernière version de jetson-inference (voir [l'issue dédiée](https://github.com/dusty-nv/ros_deep_learning/issues/14)).
On réinstallera donc jetson-inference, mais depuis le tag `L4T-R32.1`...

Ensuite :

```console
$ cd ~/catkin_ws/src
$ git clone https://github.com/dusty-nv/ros_deep_learning.git
$ catkin build
```

De là, il est possible de lancer des réseaux dans un nœud ROS, qui pourront écouter et publier sur des topics.

Avec le nœud `jetson_csi_cam` lancé, on peut envoyer l'image dans les réseaux de neurones.
Par exemple, pour utiliser `imagenet` (modèle `googlenet`) :

```console
$ rosrun ros_deep_learning imagenet /imagenet/image_in:=/csi_cam_0/image_raw _model_name:=googlenet
```

Les résultats de la classification sont alors envoyés sur le topic `/imagenet/classification`
