# Installation NVIDIA Jetson Nano

Ce dépôt explique comment configurer son environnement de travail pour remplacer le Raspberry Pi fourni initialement par une NVIDIA Jetson Nano

## Installation mécanique

Il est possible, en ré agençant un petit peu le robot, d'installer un kit de développement NVIDIA Jetson Nano à la place du Raspberry Pi initialement fourni avec le robot.
Il est conseillé d'orienter la Jetson Nano de telle façon que les différents ports (USB, Ethernet...) soient situés à l'arrière du robot.
Il restera alors la place de mettre la caméra vers l'avant à l'étage de la Jetson, afin de ne pas gêner le LIDAR.

La carte NVIDIA Jetson Nano ne possède pas de contrôleur WI-FI.
Elle dispose néanmoins d'un slot PCIe permettant d'en ajouter un.
Il faudra alorshttps://github.com/dusty-nv/ros_deep_learninghttps://github.com/dusty-nv/ros_deep_learning prévoir également des antennes pour la bonne réception su signal. 

## Installation de base pour la Jetson Nano

NVIDIA fournit des [instructions](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#intro) pour l'installation de base de la Jetson Nano.
Les lires jusqu'à la fin de la partie [Setup and First Boot](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#setup).

## Installation de ROS

Un problème majeur pour l'installation de ROS se situe au niveau des versions et des différentes compatibilité.
La [documentation](http://emanual.robotis.com/docs/en/platform/turtlebot3/raspberry_pi_3_setup/) de ROBOTIS utilisé jusqu'alors propose une installation des paquets ROS pour la version `Kinetik Kame`.
Hors, celle-ci n'est pas supportée pour `Ubuntu 18.04 LTS`, la version proposée par NVIDIA pour la Jetson Nano.

Comme il n'est pas question de changer l'os de la carte NVIDIA (car celui fourni comporte les drivers et autres logiciels propriétaires pour le bon fonctionnement de la carte), nous allons installer ROS `Melodic Morenia`, qui est normalement supporté par les paquets ROS de ROBOTIS.

### Installation de base

**Installer ROS Melodic Morenia :**

```console
# sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# apt update

# apt install ros-melodic-desktop-full
```

**Initialisation de rosdep**

```console
# rosdep init

$ rosdep update
```

**Modification .bashrc**

Ajouter à son `.bashrc` (ou autre source de shell) les lignes
```bash
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost
```

Les variables `ROS_MASTER_URI` et `ROS_HOSTNAME` seront à modifier en fonction de l'utilisation de ROS que l'on fera.

**Utilitaires supplmentaires**

Pour la construction de paquets ROS, la documentation recommande d'installer des paquets supplémentaires

```console
# apt install python-rosinstall python-rosinstall-generator python-wstool build-essential
```

On installera également le paquet permettant d'utiliser la commande `catki build` :

```console
# apt install python-catkin-tools
```

### Installation des paquets pour le robot

Les paquets pour la gestion du robot sont directement disponibles dans les dépôts

```console
# apt install ros-melodic-turtlebot3
```

On peut également les récupérer depuis leurs sources, tout en faisant bien attention de build la branche `melodic-devel` le cas échéant.
Dans notre cas, l'installation a été faite comme suit :

```console
$ cd ~/catkin_ws/src
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3.git --branch melodic-devel
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git --branch melodic-devel
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git --branch melodic-devel
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_applications_msgs.git --branch melodic-devel
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_applications.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_autorace.git
$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_deliver.git
 
$ cd ..
 
$ sudo apt install ros-melodic-joy \
                 ros-melodic-teleop-twist-keyboard \
                 ros-melodic-laser-proc \
                 ros-melodic-rgbd-launch \
                 ros-melodic-rosserial-arduino \
                 ros-melodic-rosserial-python \
                 ros-melodic-rosserial-server \
                 ros-melodic-rosserial-client \
                 ros-melodic-rosserial-msgs \
                 ros-melodic-amcl \
                 ros-melodic-map-server \
                 ros-melodic-move-base \
                 ros-melodic-urdf \
                 ros-melodic-xacro \
                 ros-melodic-compressed-image-transport \
                 ros-melodic-rqt-image-view \
                 ros-melodic-navigation \
                 ros-melodic-interactive-markers \
                 ros-melodic-slam-karto \
                 ros-melodic-cartographer \
                 ros-melodic-cartographer-ros \
                 ros-melodic-cartographer-ros-msgs \
                 ros-melodic-cartographer-rviz

$ rosdep install --from-paths src --ignore-src -r -y

$ catkin build
```

### Serveur VNC

Nous avons essayé plusieurs serveurs VNC pour permettre d'accéder au bureau de la NVIDIA Jetson à distance.
Cela ne s'est pas monté concluant, nous avons donc décider de faire plutôt du X forwarding.

### Tmux

Pour lancer des nœuds ROS, on pourra donc passer par la connexion ssh au Jetson Nano.
Dans ce cas, pour ne pas que le nœud s'éteigne à la fin de la connexion, on pourra utiliser le multiplexeur de de terminaux `tmux`.
De la documentation concernant cet outil est disponible [ici](https://wiki.picasoft.net/doku.php?id=tmux)

### Utilisation de ROS

#### Affichage du robot

L'affichage du robot ne fonctionne pas si la locale de définition des styles de virgule n'est pas la bonne.

Il faut donc ajouter à son `bashrc`

```bash
export LC_NUMERIC=en_US.UTF-8
```

#### SLAM

Voir le [fichier dédié](./SLAM.md) pour plus d'informations sur le SLAM.

#### Utilisation de la caméra

Ce [turotiel](https://www.pyimagesearch.com/2019/05/06/getting-started-with-the-nvidia-jetson-nano/) présente deux utilisations possibles des réseaux de neurones existants pour la Jetson Nano : la classification d'images et la reconnaissance.

Il propose d'installer l'environnement permettant de les faire tourner (compter 2 bonnes heures) et d'en tester deux :

* `imagenet-camera`, qui permet de faire de la classification d'image (donner avec une certaine probabilité le type d'objet se trouvant sur l'image)
* `detectnet-camera`, qui permet de faire de la détection sur des images (ex : détection de piéton).

Les deux réseaux ont été testés sur la Jetson. Le premier a tourné à 50 FPS, le second à 8.

## Two Days to a Demo

NVIDIA propose sur un [tutoriel](https://developer.nvidia.com/embedded/twodaystoademo) assez long pour prendre en main la Jetson Nano.

Voir [la page dédiée](./TwoDaysToADemo.md) pour plus d'informations.

## Utilisation de la caméra avec ROS

Les instructions pour utiliser la caméra avec ROS Jetson-inference sont disponibles sur [cette page](./camera.md)
