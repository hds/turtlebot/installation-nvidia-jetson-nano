# SLAM avec le TB3

Comme expliqué dans le [dépôt "Pour Commencer"](https://gitlab.utc.fr/hds/turtlebot/pour-commencer/blob/master/Basics.md#slam), le turltebot3 permet de faire du SLAM (Simultaneous Localization And Mapping) pour tracer une carte de l'environement dans lequel il évolue.

Il existe plusieurs méthodes de SLAM disponibles sous forme de paquets ROS.
Nous allons utiliser l'algorithme **cartographer** développé par Google, qui prend en compte pour la position du robot à la fois l'odométrie mais aussi les informations fournies par la centrale inertielle.

## Modification de cartographer

À l'usage, et après avoir vérifié les données publiées sur le topic `/flat_imu`, il s'est avéré que le nœud `/flat_world_imu_node` censé lisser les données de la centrale inertielle, publiait des fausses données (accélérations linéaires nulles par exemple).

Nous avons donc choisi de le désactiver et d'abonner cartographer directement au topic `/imu` publié par la centrale.
Pour cela, il nous faut modifier le fichier de configuration de cartographer, `~/catkin_ws/src/turtlebot3/turtlebot3_slam/launch/turtlebot3_cartographer.launch`.
On va commenter le `remap` du nœud `/cartographer_ros` et commenter tout le nœud `/flat_world_imu_node` :

```diff
@@ -29,7 +29,7 @@
         args="-configuration_directory $(find turtlebot3_slam)/config
               -configuration_basename $(arg configuration_basename)"
         output="screen">
-    <remap from="/imu" to="/flat_imu"/>
+    <!--<remap from="/imu" to="/flat_imu"/>-->
     <!--remap from="points2" to="/camera/depth/points" / -->
   </node>
 
@@ -39,8 +39,10 @@
         args="-resolution 0.05" />
 
   <!-- flat_world_imu_node -->
+  <!--
   <node pkg="turtlebot3_slam" type="flat_world_imu_node" name="flat_world_imu_node" output="screen">
     <remap from="imu_in" to="/imu" />
     <remap from="imu_out" to="/flat_imu" />
   </node>
+  -->
 </launch>
```

## Lancement du SLAM 

Une fois cartographer modifié, on peut le lancer pour faire une carte :

* Sur le robot :

```console
$ roscore
```

```console
$ roslaunch turtlebot3_bringup turtlebot3_robot.launch
```

* Sur l'ordinateur

```console
$ roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=cartographer
```

Une fenêtre `Rviz` s'ouvre alors, sur laquelle on peut voir le robot par au dessus, et les résultats du LIDAR en vert.
La carte se dessinera petit à petit.

Pour déplacer le robot :

```console
$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```

On fait ensuite visiter l'endroit au robot pour en construire une carte.
Il faut faire attention à ne pas faire de trop brusques accélérations ou décélérations, et ne pas aller trop ite pour laisser le temps au traitement de se faire (0.10 m/s max).

Lorsque l'on fait repasser le robot par un endroit où il est déjà allé, il corrige les dérives sur sa positions qui se sont créées au fur et à mesure du temps.

## Sauvegarde de la carte

La carte peut être sauvegardée de en lançant le nœud ROS suivant :

```console
$ rosrun map_server map_saver -f ~/map
```

La carte sera alors enregistrée dans les fichiers `~/map.pgm` et `~/map.yaml`.
